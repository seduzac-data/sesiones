# Data Science

## Fitting a Model to Data

---

### Content
* Finding “optimal” model parameters based on data.
* Choosing the goal for data mining.
* Classification via Mathematical Functions
* Regression via Mathematical Functions
* Class Probability Estimation and Logistic “Regression”
* Example: Logistic Regression versus Tree Induction
* Nonlinear Functions, Support Vector Machines, and Neural Networks

---


### Finding “optimal” model parameters based on data.
* Predictive modeling involves finding a model of the target variable in terms of other descriptive attributes.
  * From the data are produced both the structure of the model and the numeric “parameters” of the model.
* An alternative method is to start by specifying the structure of the model with certain numeric parameters left unspecified.
  * Then the data mining calculates

---

### Choosing the goal for data mining.
* The data miner specifies the form of the model and the attributes
* The goal of the data mining is to tune the parameters so that the model fits the data as well as possible
* This general approach is called parameter learning or parametric modeling.

---

### Classification via Mathematical Functions
<img src="http://148.217.200.109/files/4/4-1.png" width="300">
<img src="http://148.217.200.109/files/4/4-2.png" width="300">
<!-- <img src="http://148.217.200.109/files/4/4-3.png" width="300"> -->
<!-- ![Figure 4-1](http://148.217.200.109/files/4/4-1.png "Figure 4-1")
![Figure 4-2](http://148.217.200.109/files/4/4-2.png "Figure 4-2") -->
![Figure 4-3](http://148.217.200.109/files/4/4-3.png =300 "Figure 4-3")



